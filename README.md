# gnupg-deployment-tool

Automating deployment of GnuPG smartcards

Please help to improve this readme.

## The plan

The idea is to deploy GnuPG smartcards in an organization in such a way that 
each member or employee gets their own smartcard or USB token.

Users should not be bothered with initializing their smartcards or key 
management in general.  Instead keys should be generated and smartcards should 
be sent out from a centralized point.

So the plan would be something like this:

- Generate a master key for the organization that will sign every key later 
created with this tool
- Generate a key (for key certification only) for each user, with separate 
subkeys for signing, encryption and authentication
- Sign the user key with the organization master key
- Copy the 3 subkeys onto a smartcard
- Set a unique user PIN on the smartcard (communicated to the user)
- Set a unique admin PIN and a unique reset PIN on the smartcard (not 
communicated to the user to avoid user error)
- Store the key, its subkeys and the PINs in a secure environment in case we 
need them again
	- i.e. user loses smartcard but needs to be able to decrypt old messages
- Publish the public keys to an internal or external key server, WKS, you name 
it

Many steps of this list can be automated in order to save time and avoid errors.

## Implementation ideas

- POSIX compatible shell script (i.e. no bashisms)
- Python3 script
- C or C++ application, with or without GUI

Does this tool need to be compatible to non-unixlike operating systems?